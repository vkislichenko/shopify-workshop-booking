@yield('style')

<div class="page-width">
    @yield('content')
</div>

@yield('script')
