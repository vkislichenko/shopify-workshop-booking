@extends('layouts.shopify')

@section('style')
    @inline('resources/shopify/css/general.css')
    @inline('resources/shopify/css/reservation.css')
    @inline('resources/shopify/plugins/autocomplete/jquery.auto-complete.css')
@stop

@section('content')
    <div class="grid">
        <div class="grid-item">
            <div class="section-header text-center">
                <h1>Workshop Booking</h1>
            </div>

            <div id="error-wrapper" class="form-message form-message--error">
                <div class="messages"></div>
            </div>
            <div id="success-wrapper" class="form-message form-message--success">
                <div class="messages"></div>
            </div>

            <div id="reservation-wrapper" class="form-vertical">
                <form method="POST">
                    <div class="section">
                        <div class="block title">
                            Please fill out the form
                        </div>

                        @include('pages.shopify.reservation.customer')
                    </div>

                    <div class="section guests hidden"></div>

                    <div class="section">
                        <div class="block submit">
                            <div class="actions">
                                <button class="btn action-save">Book Workshop</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@stop

@section('script')
    @inline('resources/shopify/js/reservationForm.js')
    @inline('resources/shopify/plugins/autocomplete/jquery.auto-complete.min.js')
    <script>
        //Init form on page load
        document.addEventListener("DOMContentLoaded", function(event) {
            initReservationForm();
        });

        let reservation = null;
        function initReservationForm() {
            //init plugins
            initAutoCompletePlugin($);
            //init form
            const guestTemplate = @includeAsJsString(pages.shopify.reservation.guest);
            const timeTable = @json($timeTableByDay);
            reservation = new ReservationForm({
                guestTemplate: guestTemplate,
                wrapper: $('#reservation-wrapper'),
                errorWrapper: $('#error-wrapper'),
                successWrapper: $('#success-wrapper'),
                timeTable: timeTable,
            });
        }
    </script>
@stop
