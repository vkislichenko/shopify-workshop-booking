<div class="block customer grid-item">
    <!-- Customer Name -->
    <div class="grid">
            <div class="grid__item medium-up--one-quarter">
                <label for="customer-name">Customer Name</label>
            </div>
            <div class="grid__item medium-up--two-thirds">
                <input type="text" required name="reservation[customer][name]" id="customer-name" class="field-customer-name">
            </div>
            <div class="grid__item medium-up--one-twelfth">
                <span class="action-remove-customer">&times;</span>
            </div>
    </div>
    <!-- Phone -->
    <div class="grid">
        <div class="grid__item medium-up--one-quarter">
            <label for="customer-phone">Phone</label>
        </div>
        <div class="grid__item medium-up--two-thirds">
            <input type="tel" pattern="[+]*[0-9\- ()]*" required name="reservation[customer][phone]" id="customer-phone" class="field-customer-phone">
        </div>
    </div>
    <!-- WorkShop Time -->
    <div class="grid">
        <div class="grid__item medium-up--one-quarter">
            <label for="schedule-time">WorkShop Time</label>
        </div>
        <div class="grid__item medium-up--two-thirds">
            <div class="grid">
                <div class="grid__item medium-up--one-half">
                    <input type="date" id="schedule-date">
                </div>
                <div class="grid__item medium-up--one-half">
                    <select name="reservation[schedule]" id="schedule-time" required></select>
                </div>
            </div>
        </div>
    </div>

    <div class="actions">
        <button class="btn action-add-guest">Add Guest</button>
    </div>
</div>
