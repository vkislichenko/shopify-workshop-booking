<div class="block guest grid-item" data-customer-id="{id}">
    <!-- Guest Name -->
    <div class="grid">
        <div class="grid__item medium-up--one-quarter">
            <label for="guest-name-{id}">Guest Name</label>
        </div>
        <div class="grid__item medium-up--two-thirds">
            <input type="text" required name="reservation[guests][{id}][name]" id="guest-name-{id}" class="field-customer-name">
        </div>
        <div class="grid__item medium-up--one-twelfth">
            <span class="action-remove-guest">&times;</span>
        </div>
    </div>
    <!-- Email -->
    <div class="grid">
        <div class="grid__item medium-up--one-quarter">
            <label for="guest-email-{id}">Email</label>
        </div>
        <div class="grid__item medium-up--two-thirds">
            <input type="email" required name="reservation[guests][{id}][email]" id="guest-email-{id}" class="field-customer-email">
        </div>
    </div>

    <div class="actions">
        <button class="btn action-add-guest">Add Guest</button>
    </div>
</div>
