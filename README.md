#Configuration

#### App Configuration
- Go to Your partners account https://partners.shopify.com
- Create new App
- Navigate tp **App setup** and set
    - *App URL* : `<domain>`
    - *Whitelisted redirection URL(s)* : `<domain>/authenticate`
- Navigate to **Extensions**
    - Go to **Online store** -> **Manage app proxy** and set
        - **Subpath prefix** : `tools`
        - **Subpath** : `workshop-booking`
        - **Proxy URL** : `<domain>/shopify/proxy`

#### Shop Configuration
- Go to admin panel of Your Shopify Omline Store
- Navigate to **Online Store** -> **Navigation** -> **Main menu** -> **Add menu item**
- Set
    - **Name** : `Workshop Booking`
    - **Link** : `/tools/workshop-booking`

#### Laravel Configuration
- Copy .env.example as .env
- Specify database and other Laraver settings
- Specify Shopify App settings

    Copy `App credentials` from https://partners.shopify.com
    ```dotenv
    SHOPIFY_APP_NAME=
    SHOPIFY_API_KEY=
    SHOPIFY_API_SECRET=
   ```


#Install
```bash
composer install
php artisan migrate
php artisan db:seed
```
