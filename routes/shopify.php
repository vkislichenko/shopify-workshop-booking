<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;


/*
 * Shop [admin] routes
 */
Route::get('admin/workshop/booking', 'Shopify\Admin\ReservationController@index')->middleware(['auth.shop']);

/**
 * Shop [visitor,customer] proxy route
 */
Route::any('proxy', function(Request $request) {
    list($controller, $action) = explode('/', $request->input('action', 'reservation/index'));

    $classPrefix = '\App\Http\Controllers\Shopify';
    $method = sprintf('%s\%sController@%s',$classPrefix, ucfirst($controller), $action);

//    Log::info('Shopify proxy request', [
//        'controller' => $controller,
//        'action' => $action,
//        'method' => $method,
//    ]);

    return App::call($method);
})->middleware(['auth.proxy']);
