class ReservationForm {
    config = {
        guestTemplate: null,
        wrapper: null,
        errorWrapper: null,
        successWrapper: null,
        timeTable: {},
    };

    //state object contains current form values
    state = {
        customers: {},
        timeTableEvent: null,
    };

    default = {
        customer : {
            id: null,
            isLeader: 0,
            name: null,
            phone: null,
            email: null,
        }
    };
    constructor (config) {
        this.config = config;
        this.init();
    }

    init() {
        this.initEvents();
        this.showLastAddGuestButton();
        this.initGroupLeaderSection();
        this.initSchedulerElements();
    }

    initEvents() {
        this.config.wrapper.on('click', '.action-add-guest', this.addGuestSection.bind(this));
        this.config.wrapper.on('click', '.action-remove-customer', this.removeCustomer.bind(this));
        this.config.wrapper.on('click', '.action-remove-guest', this.removeGuest.bind(this));

        this.config.wrapper.on('change', '#schedule-date', this.updateSchedulerTimeOptions.bind(this));

        this.config.wrapper.on('change', '.field-customer-name', this.updateCustomerValue.bind(this, 'name'));
        this.config.wrapper.on('change', '.field-customer-phone', this.updateCustomerValue.bind(this, 'phone'));
        this.config.wrapper.on('change', '.field-customer-email', this.updateCustomerValue.bind(this, 'email'));
        this.config.wrapper.on('change', '#schedule-time', this.updateScheduleValue.bind(this));

        this.config.wrapper.on('submit', 'form', this.save.bind(this));

        // this.config.wrapper.on('keyup', '.field-customer-name', this.debounce(this.autocompleteCustomers.bind(this), 400));
    }

    initCustomerNameAutoComplete(customer, blockElement) {
        //customer name autocomplete with https://goodies.pixabay.com/jquery/auto-complete
        blockElement.find('.field-customer-name').autoComplete({
            minChars: 2,
            delay: 300,
            cache: false,
            source: (term, suggest) => {
                term = term.toLowerCase();
                this.loadCustomers(term).then(customers => {
                    suggest(customers)
                })
            },
            renderItem: (item, search) => {
                search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                let value = item.first_name+' '+item.last_name;
                return `<div class="autocomplete-suggestion" data-val="${value}" data-phone="${item.phone}" data-email="${item.email}">${value.replace(re, "<b>$1</b>")}</div>`;
            },
            onSelect: (e, term, item) => {
                //update state
                customer.name = term;
                customer.phone = item.data('phone');
                customer.email = item.data('email');
                //update gui
                blockElement.find('.field-customer-phone').val(customer.phone);
                blockElement.find('.field-customer-email').val(customer.email);
            }
        });
    }

    initGroupLeaderSection() {
        const blockElement = this.config.wrapper.find('.block.customer');
        const leader = {...this.default.customer};

        leader.id = this.uuid();
        leader.isLeader = 1;
        this.state.customers[leader.id] = leader;

        blockElement.data('customer-id', leader.id);

        this.initCustomerNameAutoComplete(leader, blockElement);
    }

    initSchedulerElements() {
        const dates = Object.keys(this.config.timeTable);
        const dateElement = this.config.wrapper.find('#schedule-date');

        //some thing went wrong, as there is no dates in timeTable
        if(!dates.length) {
            return;
        }

        const minDate = dates.shift();
        const maxDate = dates.length ? dates.pop() : minDate;

        dateElement.attr('min', minDate);
        dateElement.attr('max', maxDate);
        dateElement.val(minDate);

        this.updateSchedulerTimeOptions();
    }


    showLastAddGuestButton() {
        this.config.wrapper.find('button.action-add-guest').addClass('hidden');
        this.config.wrapper.find('button.action-add-guest:last').removeClass('hidden');
    }

    toggleCustomerRemoveButton() {
        this.config.wrapper.find('.action-remove-customer').toggle((Object.keys(this.state.customers).length > 1));
    }

    updateSchedulerTimeOptions() {
        const dates = this.config.timeTable;
        const dateElement = this.config.wrapper.find('#schedule-date');
        const timeElement = this.config.wrapper.find('#schedule-time');

        const date = dateElement.val();
        //something went wrong, as there is no date selected
        if(!date) {
            console.warn('updateSchedulerTimeOptions', 'there is no date selected');
            return;
        }

        const day = (date in dates) ? dates[date] : null;
        //something went wrong, as selected date is missing from timeTable
        if(!day) {
            console.warn('updateSchedulerTimeOptions', 'selected date is missing from timeTable', date, dates[date], Object.keys(dates).join(', '));
            return;
        }

        //remove all options
        timeElement.find('option').remove();
        //add options based on selected date
        timeElement.append("<option value=''>-- Select --</option>");
        day.forEach((element) => {
            timeElement.append(`<option value='${element.id}'>${element.time}</option>`);
        });
        //reset selected schedule
        this.state.timeTableEvent = null;
    }

    /**
     * Update State: customer name or phone
     *
     * @param field
     * @param event
     */
    updateCustomerValue(field, event) {
        //guest or customer
        let block = $(event.target).closest('.block.guest')
        if(!block.length) {
            block = $(event.target).closest('.block.customer');
        }

        const uid = block.data('customer-id');
        const customer = this.state.customers[uid];

        customer[field] = $(event.target).val();
    }

    /**
     * Update State: timeTableEvent id
     * @param event
     */
    updateScheduleValue(event) {
        this.state.timeTableEvent = parseInt($(event.target).val());
        this.validateSchedule();
    }

    addGuestSection(event) {
        event.preventDefault();
        event.stopPropagation();

        let guest = {...this.default.customer};
        guest.id = this.uuid();
        this.state.customers[guest.id] = guest;

        let html = this.config.guestTemplate;
        html = html.replace(new RegExp('{id}', 'g'), guest.id);

        this.config.wrapper.find('.guests').removeClass('hidden');
        const blockElement = this.config.wrapper.find('.guests').append(html);

        this.initCustomerNameAutoComplete(guest, blockElement);

        this.showLastAddGuestButton();
        this.toggleCustomerRemoveButton();
        this.validateSchedule();
    }

    removeCustomer() {
        event.preventDefault();
        event.stopPropagation();

        //get & delete customer
        const block = $(event.target).closest('.block.customer');
        let uid = block.data('customer-id');
        delete this.state.customers[uid];

        //some thing went wrong, as we are removing leader when there are no followers
        if(!Object.keys(this.state.customers).length) {
            return;
        }

        //reassign customer block to customer
        uid = Object.keys(this.state.customers).shift();
        const customer = this.state.customers[uid];
        customer.isLeader = 1;

        block.data('customer-id', uid);
        block.find('#customer-name').val(customer.name);
        block.find('#customer-phone').val(customer.phone);

        //remove & cleanup
        this.removeGuestSection(customer.id);
        this.showLastAddGuestButton();
        this.toggleCustomerRemoveButton();
        this.validateSchedule();
    }

    removeGuest() {
        event.preventDefault();
        event.stopPropagation();

        //get & delete customer
        const block = $(event.target).closest('.block.guest');
        const uid = block.data('customer-id');
        delete this.state.customers[uid];

        //remove & cleanup
        this.removeGuestSection(uid);
        this.showLastAddGuestButton();
        this.toggleCustomerRemoveButton();
        this.validateSchedule();
    }

    removeGuestSection(id) {
        this.config.wrapper.find('.guests .guest[data-customer-id="'+id+'"]').remove();
    }

    save(event) {
        event.preventDefault();
        event.stopPropagation();

        this.config.errorWrapper.hide();
        this.config.wrapper.find('.block.submit .action-save').attr("disabled", true);

        const url = '';
        const postData = {
            action: 'reservation/store',
            reservation: this.state
        };
        $.post(url, postData).done((data) => {
            let message = `Reservation saved. UID #${data.reservation.id}`;
            this.config.successWrapper.find('.messages').html(message);
            this.config.successWrapper.show();
            this.config.wrapper.hide();
        }).fail((xhr, status, error) => {
            this.errorHandler(xhr.status, xhr.responseJSON || {});
        }).always(() => {
            this.config.wrapper.find('.block.submit .action-save').attr("disabled", false);
        });
    }

    errorHandler(status, data) {
        let message = data.message || 'Request Failed';

        if('errors' in data && 'reservation' in data.errors) {
            message = data.errors.reservation.join("\n");
        }
        console.log('errorHandler', status, message);

        this.config.errorWrapper.find('.messages').html(message);
        this.config.errorWrapper.show();
    }

    validateSchedule() {
        this.config.errorWrapper.hide();
        this.config.wrapper.find('.block.submit .action-save').attr("disabled", false);

        if(!this.state.timeTableEvent) {
            return;
        }

        const timeTableEvent = this.getEventFromTimeTable(this.state.timeTableEvent);
        //something went wrong, as there is no such event in timeTable
        if(!timeTableEvent) {
            console.warn('validateSchedule', `there is no event with id ${this.state.timeTableEvent}`);
            return;
        }
        console.log(timeTableEvent);
        //something went wrong, as there is no info for event
        if(!timeTableEvent.info) {
            console.warn('validateSchedule', `there is no info for event with id ${this.state.timeTableEvent}`);
            return;
        }

        //invalid, not enough place
        if(Object.keys(this.state.customers).length > timeTableEvent.info.availableSpots) {
            let message = `Sorry, there are only ${timeTableEvent.info.availableSpots} more available spots for this workshop`;
            if(timeTableEvent.info.availableSpots <= 0) {
                message = 'Sorry, there are no available spots for this workshop';
            }
            this.config.errorWrapper.find('.messages').html(message);
            this.config.errorWrapper.show();
            this.config.wrapper.find('.block.submit .action-save').attr("disabled", true);
        }
        console.log(Object.keys(this.state.customers).length, timeTableEvent.info.availableSpots);

    }

    getEventFromTimeTable(id) {
        let result = null;
        for(let date in this.config.timeTable) {
            let day = this.config.timeTable[date];
            result = day.find((element)  => {
                return element.id === id;
            });
            if(result) break;
        }
        return result;
    }

    loadCustomers(search) {
        return new Promise(resolve => {
            // setTimeout(() => {
            //     resolve('resolved');
            // }, 2000);
            const url = '';
            const postData = {
                action: 'autocomplete/customers',
                search : search
            };
            $.post(url, postData).done((data) => {
                console.log('loadCustomers:response', data);
                resolve(data.customers || []);
            }).fail((xhr, status, error) => {
                console.log('loadCustomers:fail', xhr);
                this.errorHandler(xhr.status, xhr.responseJSON || {});
                resolve([]);
            }).always(() => {
                console.log('loadCustomers:completed');
            });
        });

    }

    uuid() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        )
    }

    /**
     * - Not used for now -
     */
    debounce(callback, delay) {
        let timerId;
        return function (...args) {
            if (timerId) {
                clearTimeout(timerId);
            }
            timerId = setTimeout(() => {
                callback(...args);
                timerId = null;
            }, delay);
        }
    }
    
    throttle(callback, delay) {
        let lastCall = 0;
        return function (...args) {
            const now = (new Date).getTime();
            if (now - lastCall < delay) {
                return;
            }
            lastCall = now;
            return callback(...args);
        }
    }

    getCustomerRecordByElement(element) {
        //guest or customer
        let block = element.closest('.block.guest')
        if(!block.length) {
            block = element.closest('.block.customer');
        }

        const uid = block.data('customer-id');
        const customer = this.state.customers[uid];
        return customer;
    }

    autocompleteCustomers(event) {
        const element = $(event.target);
        //guest or customer
        let block = element.closest('.block.guest')
        if(!block.length) {
            block = element.closest('.block.customer');
        }

        const uid = block.data('customer-id');
        const customer = this.state.customers[uid];
        const search = element.val();

        this.loadCustomers(search)
    }
}
