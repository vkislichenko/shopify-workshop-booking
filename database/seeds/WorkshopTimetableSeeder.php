<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class WorkshopTimetableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //June 1st 9AM - 12PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 1),
                'time_from' => Carbon::createFromTime(9),
                'time_to' => Carbon::createFromTime(12),
                'max_customer_amount' => 5
            ],
            //June 1st 12PM - 3PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 1),
                'time_from' => Carbon::createFromTime(12),
                'time_to' => Carbon::createFromTime(15),
                'max_customer_amount' => 5
            ],
            //June 1st 3PM - 6PM 7
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 1),
                'time_from' => Carbon::createFromTime(15),
                'time_to' => Carbon::createFromTime(18),
                'max_customer_amount' => 7
            ],
            //June 2nd 9AM - 12PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 2),
                'time_from' => Carbon::createFromTime(9),
                'time_to' => Carbon::createFromTime(12),
                'max_customer_amount' => 5
            ],
            //June 2nd 12PM - 3PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 2),
                'time_from' => Carbon::createFromTime(12),
                'time_to' => Carbon::createFromTime(15),
                'max_customer_amount' => 5
            ],
            //June 2nd 3PM - 6PM 12
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 2),
                'time_from' => Carbon::createFromTime(15),
                'time_to' => Carbon::createFromTime(18),
                'max_customer_amount' => 12
            ],
            //June 3rd 9AM - 12PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 3),
                'time_from' => Carbon::createFromTime(9),
                'time_to' => Carbon::createFromTime(12),
                'max_customer_amount' => 5
            ],
            //June 3rd 12PM - 3PM 7
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 3),
                'time_from' => Carbon::createFromTime(12),
                'time_to' => Carbon::createFromTime(15),
                'max_customer_amount' => 7
            ],
            //June 3rd 3PM - 6PM 5
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'date' => Carbon::createFromDate(null, 6, 3),
                'time_from' => Carbon::createFromTime(15),
                'time_to' => Carbon::createFromTime(18),
                'max_customer_amount' => 5
            ],
        ];

        foreach ($data as $index => $item) {
            $item['id'] = $index+1;
            DB::table('workshop_timetable')->updateOrInsert(
                ['id' => $item['id']],
                $item
            );
        }

    }
}
