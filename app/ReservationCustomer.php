<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationCustomer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['reservation_id', 'is_leader', 'name', 'phone', 'email'];


    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reservation() {
        return $this->belongsTo(Reservation::class);
    }
}
