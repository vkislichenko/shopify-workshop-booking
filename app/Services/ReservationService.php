<?php

namespace App\Services;


use App\Http\Requests\StoreReservationRequest;
use App\Reservation;
use App\WorkshopTimetable;
use Illuminate\Support\Facades\Log;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;

class ReservationService
{
    public function getTimeTableByDay(array $filters = []) {
        //get time table
        $timeTable = WorkshopTimetable::orderBy('date')->get();

        //gather time table data that will be used by js
        $timeTableByDay = $timeTable->mapToGroups(function ($item, $key) {
            /** @var WorkshopTimetable $item */
            return [
                $item->date->format('Y-m-d') => [
                    'id' => $item->id,
                    'time' => sprintf('%s - %s', $item->time_from->format('H:i'), $item->time_to->format('H:i')),
                    'info' => $item->shop_booking_info,
                ]
            ];
        });

        return $timeTableByDay;
    }

    public function getTimetableEventBookingInfo(WorkshopTimetable $timeTableEvent, $currentShop = false) {
        $result = (object)[
            'maxCustomerAmount' => $timeTableEvent->max_customer_amount,
            'occupiedCustomerAmount' => 0,
            'availableSpots' => $timeTableEvent->max_customer_amount,
        ];

        $reservationCollection = $timeTableEvent->reservations();
        if($currentShop) {
            $shop = ShopifyApp::shop();
            $reservationCollection->where('shop_id', $shop->id);
        }

        $reservations = $reservationCollection->get();
        foreach($reservations as $reservation) {
            $result->occupiedCustomerAmount += $reservation->customers()->count();
            $result->availableSpots = $result->maxCustomerAmount - $result->occupiedCustomerAmount;
        }
        return $result;
    }

    /**
     * @param StoreReservationRequest $request
     * @return Reservation
     */
    public function store(StoreReservationRequest $request) {
        $shop = ShopifyApp::shop();
        $data = $request->input('reservation');

        //create records
        $dataReservation = [
            'shop_id' => $shop->id,
            'workshop_timetable_id' => $data['timeTableEvent']
        ];
        /** @var Reservation $reservation */
        $reservation = Reservation::create($dataReservation);

        foreach($data['customers'] as $item) {
            $dataCustomer = [
                //'reservation_id' => $reservation->id,
                'is_leader' =>  (bool)$item['isLeader'],
                'name' =>  $item['name'],
                'phone' =>  $item['phone'],
                'email' =>  $item['email'],
            ];
            $reservation->customers()->create($dataCustomer);
        }
        return $reservation;
    }
}
