<?php

namespace App\Rules;

use App\Services\ReservationService;
use App\WorkshopTimetable;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class ReservationValidationRule implements Rule
{
    /**
     * @var integer
     */
    protected $availableSpots;

    /**
     * @var ReservationService
     */
    protected $reservationService;

    /**
     * ReservationValidationRule constructor.
     * @param ReservationService $reservationService
     */
    public function __construct(ReservationService $reservationService)
    {
        $this->reservationService = $reservationService;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $workshopTimetableId = $value['timeTableEvent'];
        $customersAmount = isset($value['customers']) ? count($value['customers']) : null;

        $timeTableEvent = WorkshopTimetable::find($workshopTimetableId);
        if(!$timeTableEvent) {
            Log::error('ReservationValidationRule: Unable to load WorkshopTimetable', ['id' => $workshopTimetableId]);
            return true;
        }

        $bookingInfo = $timeTableEvent->shop_booking_info;

        //no info eq no booking
        if(!$bookingInfo) {
            return true;
        }

        //set available spots amount, will be used in message()
        $this->availableSpots = $bookingInfo->availableSpots;

        //check if there is enough space for reservation
        return ($this->availableSpots >= $customersAmount);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $message = sprintf(
            'Sorry, there are only %d more available spots for this workshop',
            $this->availableSpots
        );

        if($this->availableSpots <= 0) {
            $message = 'Sorry, there are no available spots for this workshop';
        }

        return $message;
    }
}
