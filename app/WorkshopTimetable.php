<?php

namespace App;

use App\Services\ReservationService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WorkshopTimetable extends Model
{
    protected $table = 'workshop_timetable';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];


    /**
     * General time mutator.
     *
     * @return Carbon
     */
    public function getTime($value) {
        return (clone $this->date)->setTimeFromTimeString($value);
    }


    /**
     * Get Time From
     * @param $value
     * @return Carbon
     */
    public function getTimeFromAttribute($value) {
        return $this->getTime($value);
    }

    /**
     * Get Time To
     * @param $value
     * @return Carbon
     */
    public function getTimeToAttribute($value) {
        return $this->getTime($value);
    }

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations() {
        return $this->hasMany(Reservation::class);
    }

    public function getBookingInfoAttribute()
    {
        /** @var ReservationService $reservationService */
        $reservationService = resolve(ReservationService::class);
        return $reservationService->getTimetableEventBookingInfo($this);
    }

    public function getShopBookingInfoAttribute()
    {
        /** @var ReservationService $reservationService */
        $reservationService = resolve(ReservationService::class);
        return $reservationService->getTimetableEventBookingInfo($this, true);
    }
}
