<?php

namespace App\Http\Controllers\Shopify;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OhMyBrew\BasicShopifyAPI;

use Illuminate\Support\Facades\Log;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;

class AutocompleteController extends Controller
{

    public function customers(Request $request) {

        $query = $request->input('search');


        /** @var BasicShopifyAPI $api */
        $api = ShopifyApp::shop()->api();

        /**
         * https://help.shopify.com/en/api/reference/customers/customer#what-you-can-do-with-customer
         * https://help.shopify.com/en/api/reference/customers/customer#search
         * GET admin/api/2019-04/customers/search.json?query=Bob country:United States
         */

        //TODO: improve search syntax https://help.shopify.com/en/api/getting-started/search-syntax#search-query-syntax
        $result = $api->rest('GET', '/admin/customers/search.json', [
            'query' => $query,
            'fields' => implode(',', ['id', 'first_name', 'last_name', 'email', 'phone']),
        ]);

        if(is_object($result->errors)) {
            return response()->json([
                'success' => false,
                'errors' => $result->errors,
            ]);
        }

        $customers = $result->body->customers;

//Log::info('AutocompleteCustomers::search', [
//    'query' => $query,
//    'result' => $result
//]);

        return response()->json([
            'success' => true,
            'customers' => $customers,
        ]);
    }
}
