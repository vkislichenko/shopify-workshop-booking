<?php

namespace App\Http\Controllers\Shopify;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReservationRequest;
use App\Reservation;
use App\Services\ReservationService;
use App\WorkshopTimetable;
use Illuminate\Http\Request;
use Log;

class ReservationController extends Controller
{
    /**
     * @var ReservationService
     */
    protected $reservationService;

    /**
     * ReservationController constructor.
     * @param ReservationService $reservationService
     */
    public function __construct(ReservationService $reservationService)
    {
        $this->reservationService = $reservationService;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timeTableByDay = $this->reservationService->getTimeTableByDay();

        $data = [
            'timeTableByDay' => $timeTableByDay,
        ];

        $template = 'pages/shopify/reservation';

        $contentType = 'application/liquid';
//        $contentType = null;

        $response = response()->view($template, $data, 200);
        if($contentType) $response->header('Content-Type', $contentType);
        return $response;
    }

    /**
     * @param StoreReservationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreReservationRequest $request)
    {
        $reservation = $this->reservationService->store($request);
        $reservation->load('customers'); //reload relations

        return response()->json([
            'success' => true,
            'reservation' => $reservation->toArray(),
        ]);
    }
}
