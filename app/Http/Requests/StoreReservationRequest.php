<?php

namespace App\Http\Requests;

use App\Rules\ReservationValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ReservationValidationRule $reservationValidationRule
     * @return array
     */
    public function rules(ReservationValidationRule $reservationValidationRule)
    {
        return [
            'reservation' => [$reservationValidationRule]
        ];
    }
}
