<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OhMyBrew\ShopifyApp\Models\Shop;

class Reservation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['shop_id','workshop_timetable_id'];

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop() {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workshopTimetable() {
        return $this->belongsTo(WorkshopTimetable::class);
    }

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers() {
        return $this->hasMany(ReservationCustomer::class);
    }
}
