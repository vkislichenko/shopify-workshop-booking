<?php
use Illuminate\Support\Facades\Blade;

Blade::directive('includeAsJsString', function ($template) {
    return "<?php echo json_encode((string) view('$template')) ?>";
});
