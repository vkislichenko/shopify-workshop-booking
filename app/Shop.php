<?php

namespace App;

use OhMyBrew\ShopifyApp\Models\Shop as BaseShop;

class Shop extends BaseShop
{
    protected $table = 'shops';

    /**
     * @Relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations() {
        return $this->hasMany(Reservation::class);
    }
}
